test('It implements the loglevel API', () => {
  const { log } = require('../index')
  expect(log.trace).toBeDefined();
  expect(log.debug).toBeDefined();
  expect(log.warn).toBeDefined();
  expect(log.info).toBeDefined();
  expect(log.error).toBeDefined();
  expect(log.getLogger).toBeDefined();
  expect(log.setLevel).toBeDefined();
});

describe('Logger', () => {
  const logger = require('../logger')
  afterAll(() => {
    window.localStorage.removeItem('loglevel')
  })
  test('The log level defaults to warning', () => {
    window.localStorage.removeItem('loglevel')
    expect(logger.getDefaultLogLevel()).toEqual('warn')
  })
  test('The log level is set using a search query', () => {
    logger.testSetParams(new URLSearchParams('ll=debug'))
    expect(logger.getDefaultLogLevel()).toEqual('debug')
  })
  test('The log level falls back to previous value, if already set', () => {
    window.localStorage.setItem('loglevel', 'debug')
    expect(logger.getDefaultLogLevel()).toEqual('debug')
  })
  describe('Remote logging', () => {
    let open, send, setRequestHeader
    let originalLog, originalHandler

    beforeAll(() =>{
      // Mocking xhr
      open = jest.fn()
      send = jest.fn(function() {
        this.readyState = 4
        this.onreadystatechange()
      })
      setRequestHeader = jest.fn()
      const xhrMockClass = () => ({
        open            ,
        send            ,
        setRequestHeader,
      })
      window.XMLHttpRequest = jest.fn().mockImplementation(xhrMockClass)
    })
    afterEach(() => {
      open.mockClear()
      send.mockClear()
      setRequestHeader.mockClear()
      sessionStorage.removeItem('logger.remote')
    })

    beforeAll(() => {
      // Mocking default logger
      originalLog = jest.fn()
      originalHandler = (methodName, logLevel, loggerName) => originalLog
    })

    test('By default is disabled', () => {
      const myLogger = logger.LoggerFactory(originalHandler, false)
      const log = myLogger('logMethod', 'logLevel')
      log('message')
  
      expect(open).not.toHaveBeenCalled()
      expect(originalLog).toHaveBeenCalled()
    })

    test('is enabled using search query', () => {
      logger.testSetParams(new URLSearchParams('lr=1'))
      logger.setupLogger('http://www.example.com')
  
      const myLogger = logger.LoggerFactory(originalHandler)
      const log = myLogger('logMethod', 'logLevel')
      log('message')
  
      expect(open).toHaveBeenCalled()
      expect(send).toHaveBeenLastCalledWith(JSON.stringify({'message': 'message', 'loggerName': 'default', 'logLevel': 'logMethod', 'context': null}))
      expect(originalLog).toHaveBeenCalled()
    })

    test('supports different loggers', () => {
      logger.setupLogger('http://www.example.com')
  
      const myLogger = logger.LoggerFactory(originalHandler, true)
      const log = myLogger('logMethod', 'logLevel', 'loggerName')
      log('message')
  
      expect(open).toHaveBeenCalled()
      expect(send).toHaveBeenLastCalledWith(JSON.stringify({'message': 'message', 'loggerName': 'loggerName', 'logLevel': 'logMethod', 'context': null}))
      expect(originalLog).toHaveBeenCalled()
    })

    test('the default loggerName can be set up', () => {
      logger.setupLogger('http://www.example.com', 'newDefault')
  
      const myLogger = logger.LoggerFactory(originalHandler, true)
      const log = myLogger('logMethod', 'logLevel')
      log('message')
  
      expect(open).toHaveBeenCalled()
      expect(send).toHaveBeenLastCalledWith(JSON.stringify({'message': 'message', 'loggerName': 'newDefault', 'logLevel': 'logMethod', 'context': null}))
      expect(originalLog).toHaveBeenCalled()
    })
  })
})