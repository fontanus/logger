## Usage

The logger implements the [loglevel](https://www.npmjs.com/package/loglevel) API

- `logger.info(msg: string, data?: any)`
- `logger.debug(msg: string, data?: any)`
- `logger.warn(msg: string, data?: any)`
- `logger.error(msg: string, data?: any)`
- `logger.trace(msg: string, data?: any)`
- `logger.setLevel(msg: string, data?: any)`
- `logger.getLoggers(msg: string, data?: any)`
- `logger.getLogger(msg: string, data?: any)`

Logger adds the following features:

- using a the URL search string `lr` switches on remote logging, saved in the SessionStorage
- using the URL search string `ll=LOGLEVEL` sets all log levels to `LOGLEVEL`
- using the URL search string `lc` passes an initial context, mostly used in iframes
- creates `window.logger=loglevel`
- `logger.setupLogger`: The remote log url can be set by calling `logger.setupLogger(url, defaultLogger="default", logContextGetter=false)`
- `logger.getSetupString`: returns a query string to be passed on into iframes

## Extra API methods

### logger.setupLogger

`setupLogger(url, defaultLogger="default", logContextGetter=false)` is used set up remote logging.

- `defaultLogger`- string, specifies the `loggerName` for the default logger of `loglevel`. e.g: `com-webapp`. You can start new loggers using `logget.getLogger("mylogger")`. A recommended logger naming convention is to use `defaultLogger.myLogger` for custom loggers.
- `logContextGetter` - function or false, takes a function that returns extra data to be passed as context for the log message. By default no context is passed with the log message. If `loglevel.<method>` is called with a second data attribute that has a `context` key, then the `logContextGetter` is ignored and `data.context` is passed on as the log context.

### logger.getSetupString

`logger.getSetupString()` is used to retrieve the current logger settings as a search query url.

The primary usecase of `getSetupString` is to pass it to an iframe to retain the same remote logging and loglevel settings that the main frame has.

## Example

```js
import * as log from "@nagyv/logger"

log.setupLogger('https://www.example.com', 'example-logs', () => ({hello: 'world'}))
log.warn('A warning')
/*
posts the following to https://www.example.com when remote logging is enabled:
{
  'message': 'A warning',
  'loggerName': 'example-logs',
  'context': {
    'hello': 'world'
  }
}
*/

log.warn('A warning', {context: 'mycontext'})
/*
posts the following to https://www.example.com when remote logging is enabled:
{
  'message': 'A warning',
  'loggerName': 'example-logs',
  'context': 'mycontext',
  'data': {'context': 'mycontext'}
}
*/

const otherLog = log.getLogger('otherLog')
log.warn('A warning')
/*
posts the following to https://www.example.com when remote logging is enabled:
{
  'message': 'A warning',
  'loggerName': 'otherLog',
  'context': {
    'hello': 'world'
  }
}
*/
```
